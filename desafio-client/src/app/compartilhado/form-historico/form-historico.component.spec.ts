import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHistoricoComponent } from './form-historico.component';

describe('FormHistoricoComponent', () => {
  let component: FormHistoricoComponent;
  let fixture: ComponentFixture<FormHistoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHistoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
