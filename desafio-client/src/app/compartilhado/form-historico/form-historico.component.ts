import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Historico } from '../../interfaces/historico';


@Component({
  selector: 'app-form-historico',
  templateUrl: './form-historico.component.html',
  styleUrls: ['./form-historico.component.css']
})
export class FormHistoricoComponent {
  @Input() item: Historico = {} as Historico;
  @Output() outputHistorico: EventEmitter<Historico> = new EventEmitter();

  onSubmit() {
    this.outputHistorico.emit(this.item);
  }
}
