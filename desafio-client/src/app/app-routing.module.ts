import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaUsuariosComponent } from './paginas/lista-usuarios/lista-usuarios.component';
import { CriarUsuarioComponent } from './paginas/criar-usuario/criar-usuario.component';
import { EditarUsuarioComponent } from './paginas/editar-usuario/editar-usuario.component';
import { ListaHistoricoComponent } from './paginas/historico/lista-historico/lista-historico.component';
import { CriarItemHistoricoComponent } from './paginas/historico/criar-item-historico/criar-item-historico.component';
import { EditarItemHistoricoComponent } from './paginas/historico/editar-item-historico/editar-item-historico.component';
import { ImportarCsvComponent } from './paginas/importar-csv/importar-csv.component';


const routes: Routes = [
  {path: 'usuarios', component: ListaUsuariosComponent},
  {path: 'usuarios/criar', component: CriarUsuarioComponent},
  {path: 'usuarios/editar/:id', component: EditarUsuarioComponent},
  {path: 'historico', component: ListaHistoricoComponent},
  {path: 'historico/criar', component: CriarItemHistoricoComponent},
  {path: 'historico/editar/:id', component: EditarItemHistoricoComponent},
  {path: 'csv', component: ImportarCsvComponent},
  {path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
