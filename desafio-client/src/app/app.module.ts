import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorMsgComponent } from './compartilhado/error-msg/error-msg.component';
import { ListaUsuariosComponent } from './paginas/lista-usuarios/lista-usuarios.component';
import { FormUsuarioComponent } from './compartilhado/form-usuario/form-usuario.component';
import { CriarUsuarioComponent } from './paginas/criar-usuario/criar-usuario.component';
import { EditarUsuarioComponent } from './paginas/editar-usuario/editar-usuario.component';
import { ListaHistoricoComponent } from './paginas/historico/lista-historico/lista-historico.component';
import { FormHistoricoComponent } from './compartilhado/form-historico/form-historico.component';
import { CriarItemHistoricoComponent } from './paginas/historico/criar-item-historico/criar-item-historico.component';
import { EditarItemHistoricoComponent } from './paginas/historico/editar-item-historico/editar-item-historico.component';
import { ImportarCsvComponent } from './paginas/importar-csv/importar-csv.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorMsgComponent,
    ListaUsuariosComponent,
    FormUsuarioComponent,
    CriarUsuarioComponent,
    EditarUsuarioComponent,
    ListaHistoricoComponent,
    FormHistoricoComponent,
    CriarItemHistoricoComponent,
    EditarItemHistoricoComponent,
    ImportarCsvComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
