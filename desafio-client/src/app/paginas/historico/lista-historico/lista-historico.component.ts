import { Component, OnInit, ViewChild } from '@angular/core';
import { Historico } from '../../../interfaces/historico';
import { HistoricoService } from '../../../services/historico/historico.service';
import { ErrorMsgComponent } from '../../../compartilhado/error-msg/error-msg.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-historico',
  templateUrl: './lista-historico.component.html',
  styleUrls: ['./lista-historico.component.css']
})
export class ListaHistoricoComponent implements OnInit {

  public itens: Historico[];
  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private historicoService: HistoricoService, private router: Router) { }

  ngOnInit() {
    this.getListaItens();
  }

  getListaItens() {
    this.historicoService.getItens().subscribe((itens: Historico[]) => {
      this.itens = itens;
    }, () => { this.errorMsgComponent.setError('Falha ao buscar os itens do histórico.'); });
  }

  deletaItem(id: number) {
    this.historicoService.deletaItem(id)
      .subscribe(
        () => { this.getListaItens(); });
  }

  existemItens(): boolean {
    return this.itens && this.itens.length > 0;
  }

}
