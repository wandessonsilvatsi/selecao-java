import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarItemHistoricoComponent } from './criar-item-historico.component';

describe('CriarItemHistoricoComponent', () => {
  let component: CriarItemHistoricoComponent;
  let fixture: ComponentFixture<CriarItemHistoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriarItemHistoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarItemHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
