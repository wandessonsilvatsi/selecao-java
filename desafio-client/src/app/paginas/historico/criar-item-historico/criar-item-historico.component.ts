import { Component, ViewChild } from '@angular/core';
import { ErrorMsgComponent } from '../../../compartilhado/error-msg/error-msg.component';
import { HistoricoService } from '../../../services/historico/historico.service';
import { Router } from '@angular/router';
import { Historico } from '../../../interfaces/historico';

@Component({
  selector: 'app-criar-item-historico',
  templateUrl: './criar-item-historico.component.html',
  styleUrls: ['./criar-item-historico.component.css']
})
export class CriarItemHistoricoComponent {

  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private historicoService: HistoricoService, private router: Router) { }

  addItem(item: Historico) {
    console.log('chegou no addItem');
    this.historicoService.addItem(item).subscribe(
      () => { this.router.navigateByUrl('/historico'); },
      () => { this.errorMsgComponent.setError('Erro ao adicionar item.');
    });
  }

}
