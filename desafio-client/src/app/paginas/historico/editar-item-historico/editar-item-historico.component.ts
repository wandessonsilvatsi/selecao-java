import { Component, ViewChild, OnInit } from '@angular/core';
import { ErrorMsgComponent } from '../../../compartilhado/error-msg/error-msg.component';
import { HistoricoService } from '../../../services/historico/historico.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Historico } from '../../../interfaces/historico';

@Component({
  selector: 'app-editar-item-historico',
  templateUrl: './editar-item-historico.component.html',
  styleUrls: ['./editar-item-historico.component.css']
})
export class EditarItemHistoricoComponent implements OnInit {
  public item: Historico = {} as Historico;
  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private historicoService: HistoricoService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.getItem(this.activatedRoute.snapshot.params.id);
  }

  getItem(id: number) {
    this.historicoService.getItem(id)
      .subscribe((item: Historico) => {
        this.item = item;
      }, () => { this.errorMsgComponent.setError('Falha ao buscar usuário.'); });
  }

  updateItem(item: Historico) {
    this.historicoService.updateItem(item)
      .subscribe(
        () => { this.router.navigateByUrl('/historico'); },
        () => { this.errorMsgComponent.setError('Falha ao atualizar usuário.'); });
  }

}
