import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarItemHistoricoComponent } from './editar-item-historico.component';

describe('EditarItemHistoricoComponent', () => {
  let component: EditarItemHistoricoComponent;
  let fixture: ComponentFixture<EditarItemHistoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarItemHistoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarItemHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
