import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from '../../interfaces/usuario';
import { UsuarioService } from '../../services/usuario.service';
import { ErrorMsgComponent } from '../../compartilhado/error-msg/error-msg.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {
  public usuarios: Usuario[];
  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
    this.getListaUsuarios();
  }

  getListaUsuarios() {
    this.usuarioService.getUsuarios().subscribe((usuarios: Usuario[]) => {
      this.usuarios = usuarios;
    }, () => { this.errorMsgComponent.setError('Falha ao buscar usuários.'); });
  }

  deletaUsuario(id: number) {
    this.usuarioService.deletaUsuario(id)
      .subscribe(
        () => { this.getListaUsuarios(); });
  }

  existemUsuarios(): boolean {
    return this.usuarios && this.usuarios.length > 0;
  }

}
