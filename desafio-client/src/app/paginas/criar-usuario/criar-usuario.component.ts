import { Component, ViewChild } from '@angular/core';
import { ErrorMsgComponent } from '../../compartilhado/error-msg/error-msg.component';
import { UsuarioService } from '../../services/usuario.service';
import { Router } from '@angular/router';
import { Usuario } from '../../interfaces/usuario';

@Component({
  selector: 'app-criar-usuario',
  templateUrl: './criar-usuario.component.html',
  styleUrls: ['./criar-usuario.component.css']
})
export class CriarUsuarioComponent {
  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  addUsuario(usuario: Usuario) {
    this.usuarioService.addUsuario(usuario).subscribe(
      () => { this.router.navigateByUrl('/usuarios'); },
      () => { this.errorMsgComponent.setError('Erro ao adicionar usuário.');
    });
  }
}
