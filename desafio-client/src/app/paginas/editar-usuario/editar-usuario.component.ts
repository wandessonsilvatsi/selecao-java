import { Component, ViewChild, OnInit } from '@angular/core';
import { ErrorMsgComponent } from '../../compartilhado/error-msg/error-msg.component';
import { UsuarioService } from '../../services/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from '../../interfaces/usuario';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent implements OnInit {
  public usuario: Usuario = {} as Usuario;
  @ViewChild(ErrorMsgComponent, {static: true}) errorMsgComponent: ErrorMsgComponent;

  constructor(private usuarioService: UsuarioService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.getUsuario(this.activatedRoute.snapshot.params.id);
  }

  getUsuario(id: number) {
    this.usuarioService.getUsuario(id)
      .subscribe((usuario: Usuario) => {
        this.usuario = usuario;
      }, () => { this.errorMsgComponent.setError('Falha ao buscar usuário.'); });
  }

  updateUsuario(usuario: Usuario) {
    this.usuarioService.updateUsuario(usuario)
      .subscribe(
        () => { this.router.navigateByUrl('/usuarios'); },
        () => { this.errorMsgComponent.setError('Falha ao atualizar usuário.'); });
  }

}
