import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportarCsvComponent } from './importar-csv.component';

describe('ImportarCsvComponent', () => {
  let component: ImportarCsvComponent;
  let fixture: ComponentFixture<ImportarCsvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportarCsvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportarCsvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
