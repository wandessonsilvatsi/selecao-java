export interface Historico {
    id: number;
    combustivel: string;
    data: any;
    preco: number;
}
