import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Usuario } from '../interfaces/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  getUsuarios(): Observable<Usuario[]> {
    const url = `${environment.urlApi}/usuarios`;
    return this.http.get<Usuario[]>(url);
  }

  getUsuario(id: number): Observable<Usuario> {
    const url = `${environment.urlApi}/usuarios/${id}`;
    return this.http.get<Usuario>(url);
  }

  addUsuario(usuario: Usuario): Observable<Usuario> {
    const url = `${environment.urlApi}/usuarios`;
    return this.http.post<Usuario>(url, usuario);
  }

  updateUsuario(usuario: Usuario): Observable<Usuario> {
    const url = `${environment.urlApi}/usuarios`;
    return this.http.put<Usuario>(url, usuario);
  }

  deletaUsuario(id: number) {
    const url = `${environment.urlApi}/usuarios/${id}`;
    return this.http.delete(url);
  }
}
