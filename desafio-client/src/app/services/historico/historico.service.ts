import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Historico } from '../../interfaces/historico';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(private http: HttpClient) { }

  getItens(): Observable<Historico[]> {
    const url = `${environment.urlApi}/historico`;
    return this.http.get<Historico[]>(url);
  }

  getItem(id: number): Observable<Historico> {
    const url = `${environment.urlApi}/historico/${id}`;
    return this.http.get<Historico>(url);
  }

  addItem(item: Historico): Observable<Historico> {
    const url = `${environment.urlApi}/historico`;
    console.log(item);
    return this.http.post<Historico>(url, item);
  }

  updateItem(item: Historico): Observable<Historico> {
    const url = `${environment.urlApi}/historico`;
    return this.http.put<Historico>(url, item);
  }

  deletaItem(id: number) {
    const url = `${environment.urlApi}/historico/${id}`;
    return this.http.delete(url);
  }
}
