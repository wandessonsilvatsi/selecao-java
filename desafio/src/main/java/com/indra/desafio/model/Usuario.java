package com.indra.desafio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.indra.desafio.model.dto.UsuarioDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter 
@Setter
@NoArgsConstructor
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty
	@Column(length = 50)
	private String nome;
	
	@NotEmpty
	@Column(length = 50)
	private String login;
	
	@NotEmpty
	@Column(length = 50)
	private String senha;
	
	@NotEmpty
	@Column(length = 50)
	@Email
	private String email;
	
	private boolean admin = false;
	
	public Usuario(String nome, String login, String email, String senha) {
	    this.nome = nome;
	    this.login = login;
	    this.email = email;
	    this.senha = senha;
	}
	
	public UsuarioDTO converteParaUsuarioDTO() {
		return new UsuarioDTO(nome, login, email, senha);
	}
}
