package com.indra.desafio.model.dto;

import com.indra.desafio.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UsuarioDTO {
	private String nome;
	private String login;
	private String email;
	private String senha;
	
	public Usuario converteParaUsuario() {
		return new Usuario(nome, login, email, senha);
	}
}
