package com.indra.desafio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indra.desafio.model.Arquivo;
import com.indra.desafio.model.CombustivelCSV;
import com.indra.desafio.model.Media;
import com.indra.desafio.services.CombustivelCSVService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/combustivel")
@Api(value="API REST para consulta dos dados de combustível importados de um arquivo CSV")
@CrossOrigin(origins="*")
public class CombustivelCSVController {	
	
	@Autowired
	private CombustivelCSVService combustivelService;
	
	@ApiOperation(value = "Importa CSV com informações de combustíveis")
	@PostMapping
	public ResponseEntity <Void> importaCSV(@RequestBody Arquivo arquivo) {
		combustivelService.lerArquivoCSV(arquivo);		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	
	@ApiOperation(value = "Retorna a media de preço de combustível com base no nome de um município")
	@GetMapping(value = "/media-de-preco/{municipio}")
	public ResponseEntity<Float> mediaDePrecoPorMunicipio(@PathVariable String municipio) {
		return new ResponseEntity<Float>(this.combustivelService.mediaDePrecoPorMunicipio(municipio), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna todas as informações importadas por sigla da região")
	@GetMapping(value = "/dados-por-sigla/{sigla}")
	public ResponseEntity<List<CombustivelCSV>> dadosPorSigla(@PathVariable String sigla) {
		return new ResponseEntity<>(this.combustivelService.getDadosPorSigla(sigla), HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna todas as informações agrupadas por distribuidora")
	@GetMapping(value = "/dados-agrupados-por-distribuidora")
	public ResponseEntity<List<CombustivelCSV>> dadosAgrupadosPorDistribuidora() {
		return new ResponseEntity<>(this.combustivelService.getDadosAgrupadosPorDistribuidora(), HttpStatus.OK);
	}

	@ApiOperation(value = "Retorna todas as informações agrupadas por data de coleta")
	@GetMapping(value = "/dados-agrupados-por-data-coleta")
	public ResponseEntity<List<CombustivelCSV>> dadosAgrupadosPorDataColeta() {
		return new ResponseEntity<>(this.combustivelService.getDadosAgrupadosPorDataColeta(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna o valor médio de compra e de venda por bandeira")
	@GetMapping(value = "/valor-media-compra-venda-bandeira")
	public ResponseEntity<List<Media>> valorMedioCompraVendaBandeira() {
		return new ResponseEntity<List<Media>>(this.combustivelService.valorMedioCompraVendaBandeira(), HttpStatus.OK);
	}
	
	@ApiOperation(value = "Retorna o valor médio de compra e de venda por municipio")
	@GetMapping(value = "/valor-media-compra-venda-municipio")
	public ResponseEntity<List<Media>> valorMedioCompraVendaMunicipio() {
		return new ResponseEntity<List<Media>>(this.combustivelService.valorMedioCompraVendaMunicipio(), HttpStatus.OK);
	}

}
