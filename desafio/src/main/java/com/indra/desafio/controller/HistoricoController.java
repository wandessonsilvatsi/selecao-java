package com.indra.desafio.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indra.desafio.model.Historico;
import com.indra.desafio.services.HistoricoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/historico")
@Api(value="API REST para histórico dos preços de combustível")
public class HistoricoController {
	
	@Autowired
	private HistoricoService historicoService;
	
	@ApiOperation(value="Retorna uma lista do histórico de preços de combustível")
	@GetMapping
	public List<Historico> getAll() {
		return this.historicoService.findAll();
	}
	
	@ApiOperation(value="Retorna um único item do hisórico a partir do seu identificador")
	@GetMapping("/{id}")
	public ResponseEntity<?> listaPorId(@PathVariable(value="id") long id) {
		Optional<Historico> itemHistorico = this.historicoService.findById(id);
		
		if (itemHistorico.isPresent()) {
			return new ResponseEntity<>(itemHistorico, HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Item não encontrado no histórico", HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value="Cadastra um novo item no hisórico")
	@PostMapping
	public ResponseEntity<?> salvar(@RequestBody Historico novoItemHistorico) {
		return new ResponseEntity<>(this.historicoService.save(novoItemHistorico), HttpStatus.CREATED);
	}
	
	@ApiOperation(value="Atualiza um item do histórico")
	@PutMapping
	public ResponseEntity<?> atualizar(@RequestBody Historico itemHistorico) {
		Optional<Historico> item = this.historicoService.findById(itemHistorico.getId());
		
		if (item.isPresent()) {
			return new ResponseEntity<>(this.historicoService.update(item.get().getId(), itemHistorico), HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Item não encontrado no histórico", HttpStatus.NOT_FOUND);
	}
	
	@ApiOperation(value="Remove um item do histórico a partir do seu identificador")
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remover(@PathVariable(value="id") long id){
		Optional<Historico> itemHistorico = this.historicoService.findById(id);
		
		if (itemHistorico.isPresent()) {
			this.historicoService.delete(itemHistorico.get().getId());
			return new ResponseEntity<>("Item removido do histórico com sucesso!", HttpStatus.OK);
		}
		
		return new ResponseEntity<>("Item não encontrado no histórico", HttpStatus.NOT_FOUND);
	}
}
