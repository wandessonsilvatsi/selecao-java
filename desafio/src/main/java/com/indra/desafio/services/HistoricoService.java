package com.indra.desafio.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indra.desafio.model.Historico;
import com.indra.desafio.repository.HistoricoRepository;

@Service
public class HistoricoService {
	
	@Autowired
	private HistoricoRepository repository;
	
	public List<Historico> findAll() {
		return repository.findAll();
	}
	
	public Historico save(Historico historico) {
		return repository.save(historico);
	}

	public Optional<Historico> findById(Long id) {
		return repository.findById(id);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public Historico update(Long id, Historico historicoNovo) {
		Historico historico = repository.getOne(id);
		updateData(historico, historicoNovo);
		return repository.save(historicoNovo);
	}
	
	private void updateData(Historico historico, Historico historicoNovo) {
		historico.setCombustivel(historicoNovo.getCombustivel());
		historico.setData(historicoNovo.getData());
		historico.setPreco(historicoNovo.getPreco());
	}

}
