package com.indra.desafio.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indra.desafio.model.Usuario;
import com.indra.desafio.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	public List<Usuario> findAll() {
		return repository.findAll();
	}
	
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}

	public Optional<Usuario> findById(Long id) {
		return repository.findById(id);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public Usuario update(Long id, Usuario usuarioNovo) {
		Usuario usuario = repository.getOne(id);
		updateData(usuario, usuarioNovo);
		return repository.save(usuario);
	}
	
	private void updateData(Usuario usuario, Usuario usuarioNovo) {
		usuario.setNome(usuarioNovo.getNome());
		usuario.setEmail(usuarioNovo.getEmail());
		usuario.setLogin(usuarioNovo.getLogin());
		usuario.setSenha(usuarioNovo.getSenha());
	}

}
